//地图覆盖物加载
function mapShroud(map, mapInfo, callback) {
    let infoArr = [];
    if (null != map && null != mapInfo) {
        map.clearMap();
        for (let i = 0; mapInfo.data.length > i; i++) {
            let info = mapInfo.data[i].overlays;
            let data = info.data;
            let path = disposeArr(data.path);
            switch (info._name) {
                case 'AMap.Marker'://点
                    let marker = new AMap.Marker({
                        icon: data.icon,
                        position: [data.position.lng, data.position.lat],
                        offset: new AMap.Pixel(data.offset.x, data.offset.y),
                        animation:data.animation,
                        angle:data.angle,
                        draggable:data.draggable,
                        label:data.extData?{ content: data.extData.deploy, direction: "bottom" }:"",
                        extData:data.extData
                    });
                    marker.setMap(map)
                    infoArr.push(marker);
                    break;
                case 'AMap.Polyline'://线
                    let polyline = new AMap.Polyline({
                        path: path,
                        outlineColor: data.outlineColor,
                        borderWeight: data.borderWeight,
                        strokeColor: data.strokeColor,
                        strokeOpacity: data.strokeOpacity,
                        strokeStyle: data.strokeStyle,
                        strokeWeight: data.strokeWeight,
                        zIndex: data.zIndex,
                        extData:data.extData
                    })
                    polyline.setMap(map)
                    infoArr.push(polyline);
                    break;
                case 'AMap.Polygon'://多边形
                    let polygon = new AMap.Polygon({
                        path: path,
                        strokeColor: data.strokeColor,
                        strokeWeight: data.strokeWeight,
                        strokeOpacity: data.strokeOpacity,
                        fillOpacity: data.fillOpacity,
                        strokeStyle: data.strokeStyle,
                        fillColor: data.fillColor,
                        zIndex: data.zIndex,
                        extData:data.extData
                    })
                    polygon.setMap(map)
                    infoArr.push(polygon);
                    break;
                case 'AMap.Circle'://圆
                    let circle = new AMap.Circle({
                        center: [data.center.lng, data.center.lat],
                        radius: data.radius, //半径
                        strokeColor: data.strokeColor,
                        strokeOpacity: data.strokeOpacity,
                        strokeWeight: data.strokeWeight,
                        fillOpacity: data.fillOpacity,
                        strokeStyle: data.strokeStyle,
                        strokeDasharray: data.strokeDasharray,
                        fillColor: data.fillColor,
                        zIndex: data.zIndex,
                        extData:data.extData
                    })
                    circle.setMap(map)
                    infoArr.push(circle);
                    break;
                default:
            }
        }
        map.setFitView(infoArr)
    }
    callback(infoArr)
}
//处理坐标
function disposeArr(arr) {
    let path = [];
    if (arr) {
        for (let i = 0; arr.length > i; i++) {
            path.push([arr[i].lng, arr[i].lat]);
        }
    }
    return path;
}

export {
    mapShroud
}