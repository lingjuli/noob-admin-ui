import { getMap } from "echarts";

var gMap = {
    map:null,

    //创建地图
    createMap(doc,param) {
        gMap.map =  new AMap.Map(doc, param);
        return gMap.map;
    },
    //获取地图对象
    getMap(){
        return gMap.map;
    }
}

export { gMap }